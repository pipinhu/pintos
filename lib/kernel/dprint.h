#ifndef __DPRINTF__
#define __DPRINTF__

#include <stdarg.h>
#include <stdio.h>
#include "threads/io.h"
void xputc (void* p,char byte);

void init_printf(void* putp,void (*putf) (void*,char));

void dprintf(char *fmt, ...);
void dsprintf(char* s,char *fmt, ...);

void dformat(void* putp,void (*putf) (void*,char),char *fmt, va_list va);



#endif

